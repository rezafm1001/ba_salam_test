<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $productCategories = ProductCategory::orderBy('id', 'DESC');

        if ($request->ajax()) {
            return datatables()->of($productCategories)
                ->addColumn('name', function ($row) {
                    return $row->name;
                })
                ->addColumn('slug', function ($row) {
                    return $row->slug;
                })
                ->addColumn('actions', function ($row) {
                    $buttons = '';

                        $buttons .= '<a href="' . route('admin.product-categories.edit', $row->id) . '" class="btn"><i class="fa fa-edit"></i></a>';
                        $buttons .= '<a id="' . $row->id . '" class="btn" onclick="delete_row(' . $row->id . ')"><i class="fa fa-trash"></i></a>';
                    return $buttons;

                })
                ->addColumns(['DT_RowIndex'])
                ->rawColumns(['name', 'slug', 'actions'])
                ->addIndexColumn()
                ->filterColumn('name', function ($query, $keyword) {
                    $query->where('name', 'like', "%" . $keyword . "%");
                })

                ->make(true);
        } else {
            return view('admin-panel.productCategories.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-panel.productCategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:1|max:60',
            'status' => 'required|numeric|digits:1'
        ]);
        $productCategory = new ProductCategory();
        $productCategory->status = $request->status;
        $productCategory->name = $request->name;
        $productCategory->save();
        return redirect()->route('admin.product-categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory)
    {
        return view('admin-panel.productCategories.edit', compact('productCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, ProductCategory $productCategory)
    {
        $this->validate($request, [
            'name' => 'required|string|min:1|max:60',
            'status' => 'required|numeric|digits:1'
        ]);
        $productCategory->status = $request->status;
        $productCategory->name = $request->name;
        $productCategory->save();

        return redirect()->route('admin.product-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return array|void
     */
    public function destroy(ProductCategory $productCategory , Request $request)
    {
        if ($request->ajax()) {
            $productCategory->delete();
            return ['status' => __('msg.done_successful')];
        }else{
            return abort(404);
        }
    }
}
