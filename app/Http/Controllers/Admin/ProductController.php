<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use PHPUnit\Exception;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $products = Product::orderBy('id', 'desc');
        if ($request->ajax()) {
            return datatables()->of($products)
                ->addColumn('name', function ($row) {
                    return $row->name;
                })
                ->addColumn('category', function ($row) {
                    return $row->productCategory->name;
                })
                ->addColumn('confirm', function ($row) {
                    $confirm = $row->confirm;
                    return $confirm ? '<span  class="btn btn-success">' . __('msg.Accepted') . '</span>' : '<span  class="btn btn-warning">' . __('msg.awaiting_approval') . '</span>';
                })
                ->addColumn('status', function ($row) {
                    $status = $row->status;
                    return $status ? '<span  class="btn btn-success">' . __('msg.active') . '</span>' : '<span  class="btn btn-danger">' . __('msg.inactive') . '</span>';
                })
                ->addColumn('actions', function ($row) {
                    $buttons = '';
                    $buttons .= '<a id="' . $row->id . '" class="btn" onclick="delete_row(' . $row->id . ')"><i class="fa fa-trash"></i></a>';
                    if (!$row->confirm)
                        $buttons .= '<a id="' . $row->id . '" class="btn" onclick="confirm(' . $row->id . ')"><i class="fa fa-check"></i></a>';
                    return $buttons;

                })
                ->addColumns(['DT_RowIndex'])
                ->rawColumns(['name', 'category', 'confirm', 'status', 'actions'])
                ->filterColumn('name', function ($query, $keyword) {
                    $query->where('name', 'like', "%" . $keyword . "%");
                })
                ->addIndexColumn()
                ->make(true);
        } else {
            return view('admin-panel.products.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Request $request)
    {
            $product->delete();
    }

    public function confirmProduct($id){
        $product = Product::findOrFail($id);
        $product->confirm = true;
        $product->save();
    }
}
