<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SellerController extends Controller
{
    public function index(Request $request){
        $sellers = User::whereHas('shop')->orderBy('id', 'DESC');
        if ($request->ajax()) {
            return datatables()->of($sellers)
                ->addColumn('name', function ($row) {
                    return $row->fullName();
                })
                ->addColumn('shop_name', function ($row) {
                    return $row->shop ? $row->shop->name : '-';
                })

                ->addColumn('shop_status', function ($row) {
                    $status =$row->shop->status;
                    return $status ? '<span  class="btn btn-success">' . __('msg.active') . '</span>' : '<span  class="btn btn-danger">' . __('msg.inactive') . '</span>';
                })
                ->addColumn('actions', function ($row) {
                    $buttons = '';
                        $buttons .= '<a href="' . route('admin.sellers.edit', $row->id) . '" class="btn"><i class="fa fa-edit"></i></a>';
//                        $buttons .= '<a id="' . $row->id . '" class="btn" onclick="delete_row(' . $row->id . ')"><i class="fa fa-trash"></i></a>';
//                        $buttons .= '<a href="' . route('sellers.shop', $row->id) . '" class="btn"><i class="fa fa-shopping-basket"></i></a>';

                    return $buttons;

                })
                ->addColumns(['DT_RowIndex'])
                ->rawColumns(['name', 'shop_name', 'shop_status', 'actions'])
                ->addIndexColumn()
                ->make(true);
        } else {
            return view('admin-panel.sellers.index');
        }
    }

    public function edit(User $seller){
        $this->checkBeSeller($seller);
        return view('admin-panel.sellers.edit', compact( 'seller'));
    }

    public function update(User $seller , Request $request){
        $this->checkBeSeller($seller);
        $this->validate($request, [
            'status' => 'required|numeric|digits:1',
        ]);
        $seller->shop->status = $request->status;
        $seller->shop->save();
        return redirect()->route('admin.sellers.index');
    }



    private function checkBeSeller(User $user){
        if (!$user->shop)
            return abort(404);
    }
}
