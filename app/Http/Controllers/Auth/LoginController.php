<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectTo()
    {
        if (auth()->user()->is_admin){
            session()->push('message',__('msg.welcome'));
            return route('admin.home');
        }

        else if (auth()->user()->shop){
            if (auth()->user()->shop->status){
                session()->push('message',__('msg.welcome_seller'));
                return route('seller.home');
            }else {
                session()->push('message',__('msg.for_login_to_panel_confirm_needed'));
                return '/';
            }

        }

        else
            return '/profile/home ';
    }
}
