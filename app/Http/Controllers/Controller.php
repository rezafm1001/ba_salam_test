<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function uploader($file,$directory=null){

        $mime=explode('.',$file->getClientOriginalName());
        $mime=end($mime);
        $directory=$directory==null?'users':$directory;
        $filename=time().'-'.rand(1000,9999).'.'.$mime;
        $path=public_path('/images/'.$directory);
        $file->move($path,$filename);
        return'/images/'.$directory.'/'.$filename;
    }
}
