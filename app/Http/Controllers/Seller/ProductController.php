<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateProductRequest;
use App\Models\Product;
use App\Models\ProductCategory;
use App\repositories\IProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public $product;

    public function __construct(IProductRepository $product)
    {
        $this->product = $product;
    }

    public function index(Request $request)
    {
        $products = $this->product->getAll();
        if ($request->ajax()) {
            return datatables()->of($products)
                ->addColumn('name', function ($row) {
                    return $row->name;
                })
                ->addColumn('category', function ($row) {
                    return $row->productCategory->name;
                })
                ->addColumn('confirm', function ($row) {
                    $confirm = $row->confirm;
                    return $confirm ? '<span  class="btn btn-success">' . __('msg.Accepted') . '</span>' : '<span  class="btn btn-warning">' . __('msg.awaiting_approval') . '</span>';
                })
                ->addColumn('status', function ($row) {
                    $status = $row->status;
                    return $status ? '<span  class="btn btn-success">' . __('msg.active') . '</span>' : '<span  class="btn btn-danger">' . __('msg.inactive') . '</span>';
                })
                ->addColumn('actions', function ($row) {
                    $buttons = '';
                    $buttons .= '<a href="' . route('seller.products.edit', $row->id) . '" class="btn"><i class="fa fa-edit"></i></a>';
                    $buttons .= '<a id="' . $row->id . '" class="btn" onclick="delete_row(' . $row->id . ')"><i class="fa fa-trash"></i></a>';

                    return $buttons;

                })
                ->addColumns(['DT_RowIndex'])
                ->rawColumns(['name', 'category', 'confirm', 'status', 'actions'])
                ->filterColumn('name', function ($query, $keyword) {
                    $query->where('name', 'like', "%" . $keyword . "%");
                })
                ->addIndexColumn()
                ->make(true);
        } else
            return view('seller-panel.products.index');
    }

    public function create()
    {
        $productCategories = $this->product->getProductCategories();
        return view('seller-panel.products.create', compact('productCategories'));
    }

    public function store(StoreUpdateProductRequest $request)
    {
        $image = $request->file('image') ? $this->uploader($request->file('image'), 'products') : '/images/statics/product-default.png';
        $collection = $request->except(['_token', '_method']);
        $collection['image'] = $image;
        $this->product->create($collection);
        return redirect()->route('seller.products.index');
    }

    public function edit(Product $product)
    {
        $this->checkBeOwnProduct($product);
        $productCategories = $this->product->getProductCategories();
        return view('seller-panel.products.edit', compact('productCategories', 'product'));
    }

    public function update(StoreUpdateProductRequest $request, Product $product)
    {
        $this->checkBeOwnProduct($product);
        $collection = $request->except(['_token', '_method']);
        $collection['image'] = $this->updateImage($product, $request->file('image'));
        $this->product->update($product, $collection);
        return redirect()->route('seller.products.index');
    }

    public function destroy(Product $product, Request $request)
    {
        $this->checkBeOwnProduct($product);
        $this->product->delete($product);
    }

    private function checkBeOwnProduct(Product $product)
    {
        if ($product->creator_id != auth()->user()->id)
            return abort(401);
    }


    //in update method if a new image upload we remove first image if this was not default
    private function updateImage($product, $file = null)
    {
        if ($file) {
            if ($product->image != '/images/statics/product-default.png') {
                try {
                    unlink(public_path($product->image));
                } catch (\Exception $e) {
                }
            }
            return $this->uploader($file, 'products');
        }
        return $product->image;
    }
}
