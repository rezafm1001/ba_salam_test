<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next , $type)
    {
        //this method check any users just see them routes
        if ($type == 'admin'){
            if (auth()->user()->is_admin)
                return $next($request);
            else
                return abort(401 , __('msg.401_message'));
        }elseif ($type == 'seller'){
            //if user has a shop relation so he is a seller
            //he can do any action when him status is true
            if (auth()->user()->shop && auth()->user()->shop->status)
                return $next($request);
            else
                return abort(401 , __('msg.401_message'));
        }else
            return $next($request);
    }
}
