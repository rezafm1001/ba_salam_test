<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'price' => 'required|numeric',
            'count' => 'required|numeric',
            'discount' => 'nullable|numeric',
            'description' => 'nullable|string',
            'status' => 'required|numeric|digits:1',
            'category' => 'required|numeric',
            'image' => 'nullable|mimes:jpeg,bmp,png,jpg|max:4096',
        ];
    }
}
