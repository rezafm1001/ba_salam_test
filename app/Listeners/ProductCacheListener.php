<?php

namespace App\Listeners;

use App\Models\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ProductCacheListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     * @throws \Exception
     */
    public function handle($event)
    {
        cache()->forget('products');
        $products = Product::where('status',true)->where('confirm',true)->whereHas('creator',function ($q){
            $q->whereHas('shop',function ($q){
                $q->where('status',true);
            });
        })->get();
        cache()->forever('products', $products);
    }
}
