<?php

namespace App\Models;

use App\Events\ProductChangeEvent;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PHPUnit\Exception;

class Product extends Model
{
    use SoftDeletes , Sluggable;
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    protected static function boot() {
        parent::boot();
        static::deleting(function($product) {

                if ($product->image != '/images/statics/product-default.png') {
                    try {
                        unlink(public_path($product->image));
                    } catch (Exception $e) {

                    }
                }
        });
    }
    protected $dispatchesEvents = [
        'created'=>ProductChangeEvent::class,
        'updated'=>ProductChangeEvent::class,
        'deleted'=>ProductChangeEvent::class,
    ];
    protected $guarded=[];

    public function productCategory(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ProductCategory::class , 'product_category_id');
    }

    public function creator(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class , 'creator_id');
    }
}
