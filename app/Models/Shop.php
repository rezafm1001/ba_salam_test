<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;
    protected $fillable = ['national_code','mobile','address','postal_code','shop_name','shaba_code','status','seller_id',];

    public function seller(){
        return $this->belongsTo(User::class , 'seller_id');
    }
}
