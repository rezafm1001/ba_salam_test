<?php


namespace App\repositories;


use App\Models\Product;

interface IProductRepository
{
    //seller methods
    public function getAll();
    public function create($collection = []);
    public function update(Product $product , $collection = []);
    public function delete(Product $product);
    public function getProductCategories();


}
