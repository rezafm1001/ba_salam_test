<?php


namespace App\repositories;


use App\Models\Product;
use App\Models\ProductCategory;

class ProductRepository implements IProductRepository
{


    public function getAll()
    {
        return auth()->user()->products()->orderBy('id', 'desc');
    }



    public function create($collection = [])
    {
        $product = new Product();
        $product->name = $collection['name'];
        $product->price = $collection['price'];
        $product->count = $collection['count'];
        $product->discount = $collection['discount'] ? $collection['discount'] : 0;
        $product->description = $collection['description'];
        $product->status = $collection['status'];
        $product->creator_id = auth()->user()->id;
        $product->product_category_id = $collection['category'];
        $product->image = $collection['image'];
        return $product->save();
    }

    public function update(Product $product, $collection = [])
    {
        $product->name = $collection['name'];
        $product->price = $collection['price'];
        $product->count = $collection['count'];
        $product->discount = $collection['discount'] ? $collection['discount'] : 0;
        $product->description = $collection['description'];
        $product->status = $collection['status'];
        $product->confirm = false;
        $product->creator_id = auth()->user()->id;
        $product->product_category_id = $collection['category'];
        $product->image = $collection['image'];
        return $product->save();
    }

    public function delete($product)
    {
        $product->status = false;
        return $product->save();
    }

    public function getProductCategories()
    {
       return ProductCategory::where('status', true)->get();
    }
}
