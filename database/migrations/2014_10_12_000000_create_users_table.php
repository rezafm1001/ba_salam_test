<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('fname', 100)->nullable()->comment('نام');
            $table->string('lname', 100)->nullable()->comment('نام خانوادگی');
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('is_admin')->default(false)->comment('آیا ادمین است؟');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->unique(["email", "deleted_at"]);
        });

        $tbl_comment = 'جدول کاربران';
        DB::statement('ALTER TABLE `users` comment "' . $tbl_comment .'"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
