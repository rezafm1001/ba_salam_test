<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->id();
            $table->string('national_code', 10)->nullable()->comment('کدملی غرفه دار');
            $table->string('mobile', 11)->nullable()->comment('موبایل غرفه دار');
            $table->string('address')->nullable()->comment('آدرس دقیق');
            $table->string('postal_code', 12)->nullable()->comment('کد پستی');
            $table->string('shop_name')->comment('نام غرفه')->nullable();
            $table->string('shaba_code')->comment('شماره شبا')->nullable();
            $table->boolean('status')->default(false)->comment('تایید شده');
            $table->unsignedBigInteger('seller_id')->comment('آیدی غرفه دار');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(["mobile", "deleted_at"]);
            $table->foreign('seller_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
        $tbl_comment = 'جدول غرفه ها';
        DB::statement('ALTER TABLE `shops` comment "' . $tbl_comment .'"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
