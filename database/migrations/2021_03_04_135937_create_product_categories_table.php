<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
            Schema::create('product_categories', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('creator_id')->nullable()->comment('شناسه ثبت کننده');
                $table->string('name')->comment('نام');
                $table->string('slug');
                $table->boolean('status')->default(true)->comment('وضعیت');

                $table->unique(['slug','deleted_at']);
                $table->timestamps();
                $table->softDeletes();
            });
            $tbl_comment = 'جدول دسته بندی محصولات';
            DB::statement('ALTER TABLE `product_categories` comment "' . $tbl_comment .'"');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categories');
    }
}
