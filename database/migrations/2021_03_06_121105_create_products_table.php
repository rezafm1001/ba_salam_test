<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('creator_id')->comment('آیدی فروشنده')->nullable();
            $table->unsignedBigInteger('product_category_id')->comment('شناسه دسته بندی');
            $table->string('image', 150)->nullable()->comment('تصویر');
            $table->string('name');
            $table->string('slug');
            $table->boolean('status')->default(1)->comment('وضعیت');
            $table->boolean('confirm')->default(0)->comment('تایید شده');
            $table->text('description')->nullable()->comment('توضیحات');
            $table->bigInteger('price')->default(0)->comment('قیمت پایه');
            $table->bigInteger('discount')->default(0)->comment('تحفیف پایه');
            $table->integer('count')->default(0)->comment('تعداد');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['slug','deleted_at']);
            $table->foreign('product_category_id')->references('id')->on('product_categories')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('set null');
        });
        $tbl_comment = 'جدول محصولات';
        DB::statement('ALTER TABLE `products` comment "' . $tbl_comment .'"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
