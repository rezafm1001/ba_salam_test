<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RequiredSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'fname'=>'Reza',
            'lname'=>'Farahani',
            'email'=>'rezafm1001@gmail.com',
            'password'=>Hash::make('123456'),
            'email_verified_at'=>now(),
            'is_admin'=>true,
        ]);
    }
}
