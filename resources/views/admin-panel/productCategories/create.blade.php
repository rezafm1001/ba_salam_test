@extends('layouts.panel.menu')

@section('header')
    <section class="content-header">
        <h1>
            {{__('msg.product_category_create')}}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-roles"></i>{{__('msg.home')}}</a></li>
            <li class="active">{{__('msg.product_category')}}</li>
        </ol>
        @if($errors->any())
            <br><br>
            <ul>
                @foreach($errors->all() as $e)
                    <li class="alert-danger ">{{$e}}</li>
                @endforeach
            </ul>
        @endif
    </section>
@endsection
@section('content')
    <div class="box box-warning">

        <!-- /.box-header -->
        <div class="box-body">

            <!-- text input -->


            <form role="form" method="post" action="{{route('admin.product-categories.store')}}">
                @csrf

                <div class="row align-items-end col-12">
                    <div class="card-body d-flex flex-column flex-md-row align-items-md-end p-2">

                        <div class="form-group col-md-6 mx-2">
                            <label for="name">{{__('msg.title')}} </label>
                            <input class="form-control" name="name" placeholder="{{__('msg.title')}}"
                                   value="{{old('name')}}"/>
                        </div><!-- /.form-group -->
<br>
                        <div class="form-group col-md-6 mx-2">
                            <label> {{__('msg.status')}} :
                                {{__('msg.active')}} <input type="radio" name="status" class="flat-red" checked
                                                              value="1">
                            </label>
                            <label>
                                {{__('msg.inactive')}} <input type="radio" name="status" class="flat-red" value="0">
                            </label>
                        </div>

                    </div>
                </div>



                <div class="input-group-btn">
                    <input type="submit" value="{{__('msg.submit')}}" class="btn btn-success" style="float: left">
                    <a href="{{route('admin.product-categories.index')}}" class="btn btn-danger"
                       style="float: left">{{__('msg.cancel')}}</a>
                </div>


            </form>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

