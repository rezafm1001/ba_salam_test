@extends('layouts.panel.menu')
@section('header')
    <section class="content-header">
        <h1>
            {{__('msg.product_categories_table')}}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-roles"></i>{{__('msg.home')}}</a></li>
            <li class="active">{{__('msg.product_category')}}</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <a class="btn btn-info" href="{{route('admin.product-categories.create')}}">{{__('msg.create')}}</a>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="table_id" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{__('msg.row')}}</th>
                            <th>{{__('msg.category_name')}}</th>
                            <th>{{__('msg.slug')}}</th>
                            <th>{{__('msg.actions')}}</th>

                        </tr>
                        </thead>


                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="/admin-panel/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin-panel/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(window).on('load', function () {
            var Selector = {
                datatable: '#table_id',
                tableBody: '#table_id tbody',
                datatableLength: '#table_id_length',
                dtPagination: '.dataTables_paginate',
                dtPaginateButton: '.paginate_button',
                dtIdFilter: '#table_id_filter',

            }
            var State = {$isClickedOnPopoverButton: true}

            $(Selector.datatable + ' thead tr').clone(true).appendTo(Selector.datatable + ' thead');
            $(Selector.datatable + ' thead tr:eq(1) th').empty();
            var $table = $(Selector.datatable).DataTable({
                paging: true,
                searching: true,
                ordering: false,
                stateSave: true,
                serverSide: true,
                processing: true,
                orderCellsTop: true,
                ajax: {
                    url: '{{route('admin.product-categories.index')}}',
                    type: 'get',
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        class: 'text-center',
                        sortable: false,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name',
                        class: 'text-center',
                        sortable: false,
                        searchable: true
                    },
                    {
                        data: 'slug',
                        name: 'slug',
                        class: 'text-center',
                        sortable: false,
                        searchable: false
                    },
                    {
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                        sortable: false,
                        searchable: false
                    },

                ],
                pageLength: 10,
                lengthMenu: [
                    [5, 10, 25, 50, -1], // https://datatables.net/extensions/buttons/examples/initialisation/pageLength.html
                    [' 5', '10', '25', '50', '']
                ],
                initComplete: function () {
                    this.api().columns().every(function (index) {
                        var $column = this;
                        var numberOfColumn = $column.selector.cols;
                        var isSearchable = this.context[0].aoColumns[numberOfColumn].bSearchable;
                        if (isSearchable) {
                            var input = document.createElement("input");
                            $(input).addClass('form-control');
                            $(input).appendTo($(Selector.datatable + ' thead tr:eq(1) th:eq(' + numberOfColumn + ')').empty())
                                .on('input', function () {
                                    $column.search($(this).val(), true, false, true).draw();
                                });

                        } // if(isSearchable)
                    });
                },
                // dom: 'Bfrtip',
                dom: '<"top"Blp>rti<"clear">',
            }); // var table
            $('<div class="dataTable_wrapper-style bg-white"></div>')
                .append($(Selector.datatable))
                .insertAfter($(Selector.datatable + '_wrapper div.dataTables_processing'));

            $('<div class="dataTable_footer-style d-flex flex-column justify-content-center align-items-center px-2 py-3 bg-white"></div>')
                .append($(Selector.dtPagination).addClass('p-0 mb-2'))
                .append($(Selector.dtInfo).addClass('p-0'))
                .insertAfter($(Selector.datatable + '_wrapper .dataTable_wrapper-style'));

        });


        function delete_row(id) {
            event.preventDefault();
            var $data = {
                _token: '{{csrf_token()}}',
            };
            $.ajax({
                type: "DELETE",
                url: window.location.href+'/' + id,
                data: $data,
                success: function (res) {
                    $('#table_id').DataTable().ajax.reload();
                    // window.location.reload();
                    },
                error: function (e) {

                }
            });
        }

    </script>
@endsection
