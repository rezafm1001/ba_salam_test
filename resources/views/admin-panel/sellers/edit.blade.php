@extends('layouts.panel.menu')
@section('css')
@endsection
@section('header')
    <section class="content-header">
        <h1>
            {{__('msg.see_and_confirm')}}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-sellers"></i>{{__('msg.home')}}</a></li>
            <li class="active">{{__('msg.sellers')}}</li>
        </ol>
        @if($errors->any())
            <br><br>
            <ul>
                @foreach($errors->all() as $e)
                    <li class="alert-danger ">{{$e}}</li>
                @endforeach
            </ul>
        @endif
    </section>
@endsection
@section('content')
    <div class="box box-warning">

        <!-- /.box-header -->
        <div class="box-body">

            <!-- text input -->


            <form role="form" method="post" action="{{route('admin.sellers.update',['seller'=>$seller->id])}}"
                  enctype="multipart/form-data">
                @csrf
                {{method_field('PUT')}}
                <div class="row align-items-end col-12">
                    <div class="card-body d-flex flex-column flex-md-row align-items-md-end p-2">

                        <div class="form-group col-md-6 mx-2">
                            <label for="fname">{{__('msg.fname')}} </label>
                            <input class="form-control"  readonly placeholder="{{__('msg.fname')}}"
                                   value="{{$seller->fname}}"/>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6 mx-2">
                            <label for="lname">{{__('msg.lname')}} </label>
                            <input class="form-control"  readonly placeholder="{{__('msg.lname')}}"
                                   value="{{$seller->lname}}"/>
                        </div><!-- /.form-group -->


                        <div class="form-group col-md-6 mx-2">
                            <label for="email">{{__('msg.email')}} </label>
                            <input class="form-control" type="email" readonly placeholder="{{__('msg.email')}}"
                                   value="{{$seller->email}}"/>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6 mx-2">
                            <label for="mobile">{{__('msg.mobile')}} </label>
                            <input class="form-control" readonly placeholder="{{__('msg.mobile')}}"
                                   value="{{$seller->shop->mobile}}"/>
                        </div><!-- /.form-group -->


                        <div class="form-group col-md-6 mx-2">
                            <label for="national_code">{{__('msg.national_code')}} </label>
                            <input class="form-control" readonly
                                   placeholder="{{__('msg.national_code')}}" value="{{$seller->shop->national_code}}"/>
                        </div><!-- /.form-group -->


                        <div class="form-group col-md-6 mx-2">
                            <label for="address">{{__('msg.address')}} </label>
                            <input class="form-control" readonly placeholder="{{__('msg.address')}}"
                                   value="{{$seller->shop->address}}"/>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6 mx-2">
                            <label for="postal_code">{{__('msg.postal_code')}} </label>
                            <input class="form-control" readonly placeholder="{{__('msg.postal_code')}}"
                                   value="{{$seller->shop->postal_code}}"/>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6 mx-2">
                            <label for="shaba_code">{{__('msg.shaba_code')}} </label>
                            <input class="form-control" readonly placeholder="{{__('msg.shaba_code')}}"
                                   value="{{$seller->shop->shaba_code}}"/>
                        </div><!-- /.form-group -->


                        <div class="form-group col-md-6 mx-2">
                            <label for="shop_name">{{__('msg.shop_name')}} </label>
                            <input class="form-control"  readonly placeholder="{{__('msg.shop_name')}}"
                                   value="{{$seller->shop->shop_name}}"/>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6 mx-2">
                            <label> {{__('msg.shop_status')}} :
                                {{__('msg.active')}} <input type="radio" name="status" class="flat-red"
                                                              @if($seller->shop->status) checked @endif value="1">
                            </label>
                            <label>
                                {{__('msg.inactive')}} <input type="radio" @if(!$seller->shop->status) checked
                                                                @endif name="status" class="flat-red" value="0">
                            </label>
                        </div>



                    </div>

                </div>
                <br><br><br><br>
                <div class="input-group-btn">
                    <input type="submit" value="{{__('msg.submit')}}" class="btn btn-success" style="float: left">
                    <a href="{{route('admin.sellers.index')}}" class="btn btn-danger"
                       style="float: left">{{__('msg.cancel')}}</a>
                </div>


            </form>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

