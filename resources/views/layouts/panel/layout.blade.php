<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="">
    <title>داشبرد | {{Illuminate\Support\Facades\Config::get('app.name')}} </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/admin-panel/dist/css/bootstrap-theme.css">
    <!-- Bootstrap rtl -->
    <link rel="stylesheet" href="/admin-panel/dist/css/rtl.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/admin-panel/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/admin-panel/bower_components/Ionicons/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/admin-panel/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/admin-panel/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/admin-panel/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="/admin-panel/bower_components/select2/dist/css/select2.min.css">
@yield('css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- customize adminlte -->
    <link rel="stylesheet" href="/admin-panel/dist/css/customize-adminlte.css">



</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{route('admin.home')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->

            <span class="logo-mini">پنل</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>کنترل پنل مدیریت</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/images/statics/user.png" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{__('msg.user-info')}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="/images/statics/user.png" class="img-circle" alt="User Image">

                                <p>
                                    {{auth()->user()->email}}
                                    <small>
{{--                                        @foreach(auth()->user()->roles as $role)--}}
{{--                                        {{$role->name." "}}--}}
{{--                                        @endforeach--}}
                                    </small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                @if(auth()->user()->is_admin)
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat">{{__('msg.profile')}}</a>
                                </div>
                                @endif
{{--                                <div class="pull-right" style="margin-right: 10px !important;">--}}
{{--                                    <a href="{{auth()->user()->is_admin?route('edit-password'):route('seller-edit-password')}}" class="btn btn-default btn-flat">{{__('msg.edit_password')}}</a>--}}
{{--                                </div>--}}
                                <div class="pull-left">
                                    <a href="{{ route('logout') }}" class="btn btn-default btn-flat"   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">  {{ __('msg.logout') }}</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>

                        <a href="#" data-toggle="control-sidebar"></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- right side column. contains the logo and sidebar -->
@yield('menu')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        @yield('header')

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
      @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
{{--    <footer class="main-footer text-left">--}}
{{--        <strong>Copyleft &copy; 2019-2020 <a href="https://webkima.com">Webkima Academy</a> & <a--}}
{{--                href="https://webkima.com">Nabi Abdi</a></strong>--}}
{{--    </footer>--}}


    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="/admin-panel/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/admin-panel/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/admin-panel/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/admin-panel/dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="/admin-panel/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="/admin-panel/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/admin-panel/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="/admin-panel/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="/admin-panel/bower_components/Chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/admin-panel/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/admin-panel/dist/js/demo.js"></script>
<script src="/admin-panel/bower_components/select2/dist/js/select2.full.min.js"></script>
<script>
    $.ajaxSetup({
        beforeSend: function (xhr)
        {
            xhr.setRequestHeader("X-Requested-With","XMLHttpRequest");

        }
    });
</script>
<script>

    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
    });
</script>
<script>
    var $data = {
        _token: window.laravel.token,
    };
    $('#div_connection').empty().append(' <a href="#"><i class="fa fa-circle text-success"></i> آنلاین </a>');
  var timeOut=setInterval(function () {
      $.ajax({
          type: 'POST',
          url: window.laravel.prefix + '/check-connection',
          data: $data,
          success: function (res) {
              $('#div_connection').empty().append(' <a href="#"><i class="fa fa-circle text-success"></i> آنلاین </a>');

          },
          error: function (error) {
              $('#div_connection').empty().append(' <a href="#"><i class="fa fa-circle text-danger"></i> آفلاین </a>');
          }
      });
  },60000);


</script>
@yield('script')
</body>

</html>
