@extends('layouts.panel.layout')
@section('menu')
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-right image">
                    <img src="/images/statics/user.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-right info">
                    <p>{{auth()->user()->email}}</p>
                    <div id="div_connection">

                    </div>

                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">{{__('msg.menu')}}</li>


                <li>
                    <a href="{{auth()->user()->is_admin?route('admin.home'):route('seller.home')}}">
                        <i class="fa fa-dashboard"></i> <span>{{__('msg.dashboard')}}</span>
                        <span class="pull-left-container">
              </span>
                    </a>
                </li>
                @if(auth()->user()->is_admin)
                        <li class=" treeview">
                            <a href="#">
                                <i class="fa fa-users"></i> <span>{{__('msg.users_menu')}}</span>
                                <span class="pull-left-container"><i class="fa fa-angle-right pull-left"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                    <li @if(request()->route()->named('admin.sellers.index')) class="active" @endif>
                                        <a href="{{route('admin.sellers.index')}}"><i
                                                class="fa fa-circle-o"></i>{{__('msg.sellers_list')}} </a>
                                    </li>
                            </ul>
                        </li>

                    <li class=" treeview">
                        <a href="#">
                            <i class="fa fa-users"></i> <span>{{__('msg.product_menu')}}</span>
                            <span class="pull-left-container"><i class="fa fa-angle-right pull-left"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if(request()->route()->named('admin.product-categories.index')) class="active" @endif>
                                <a href="{{route('admin.product-categories.index')}}"><i
                                        class="fa fa-circle-o"></i>{{__('msg.product_categories_list')}} </a>
                            </li>

                            <li @if(request()->route()->named('admin.products.index')) class="active" @endif>
                                <a href="{{route('admin.products.index')}}"><i
                                        class="fa fa-circle-o"></i>{{__('msg.products_list')}} </a>
                            </li>
                        </ul>
                    </li>



                @elseif(auth()->user()->shop && auth()->user()->shop->status)
                        <li class=" treeview">
                            <a href="#">
                                <i class="fa fa-product-hunt"></i> <span>{{__('msg.products_menu')}}</span>
                                <span class="pull-left-container"><i class="fa fa-angle-right pull-left"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                <li @if(request()->route()->named('seller.products.index')) class="active" @endif>
                                    <a href="{{route('seller.products.index')}}"><i
                                            class="fa fa-circle-o"></i>{{__('msg.products')}} </a>
                                </li>

                            </ul>
                        </li>

                @endif
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
@endsection
@section('script')

@endsection
