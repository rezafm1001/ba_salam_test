@extends('layouts.panel.menu')
@section('header')
    <section class="content-header">
        <h1>
            {{__('msg.home')}}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-admins"></i>{{__('msg.home')}}</a></li>
        </ol>

        @if(session()->has('message'))
            <br><br>
            <li class="alert-success ">{{session()->pull('message')[0]}}</li>

        @endif
    </section>
@endsection
@section('content')



@endsection
