@extends('layouts.panel.menu')
@section('css')
    <link rel="stylesheet" href="/admin-panel/bower_components/select2/dist/css/select2.min.css">
@endsection
@section('header')
    <section class="content-header">
        <h1>
            {{__('msg.product_edit')}}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-roles"></i>{{__('msg.home')}}</a></li>
            <li class="active">{{__('msg.products')}}</li>
        </ol>
        @if($errors->any())
            <br><br>
            <ul>
                @foreach($errors->all() as $e)
                    <li class="alert-danger ">{{$e}}</li>
                @endforeach
            </ul>
        @endif
    </section>
@endsection
@section('content')
    <div class="box box-warning">

        <!-- /.box-header -->
        <div class="box-body">

            <!-- text input -->


            <form role="form" method="post" action="{{route('seller.products.update',['product'=>$product->id])}}"
                  enctype="multipart/form-data">
                @csrf
                {{method_field('PUT')}}
                <div class="row align-items-end col-12">
                    <div class="card-body d-flex flex-column flex-md-row align-items-md-end p-2">

                        <div class="form-group col-md-6 mx-2">
                            <label for="name">{{__('msg.title')}} </label>
                            <input class="form-control" name="name" placeholder="{{__('msg.title')}}"
                                   required value="{{$product->name}}"/>
                        </div><!-- /.form-group -->


                        <div class="form-group col-md-6 mx-2">
                            <label for="price">{{__('msg.main_price') . ' (' . __('msg.currency') . ') '}} </label>
                            <input class="form-control" type="number" name="price" required
                                   placeholder="{{__('msg.main_price')}}"
                                   value="{{$product->price}}"/>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6 mx-2">
                            <label for="discount">{{__('msg.discount')}} </label>
                            <input class="form-control" name="discount" type="number"
                                   placeholder="{{__('msg.discount')}}"
                                   value="{{$product->discount}}"/>
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6 mx-2">
                            <label for="count">{{__('msg.count')}} </label>
                            <input class="form-control" required name="count" type="number"
                                   placeholder="{{__('msg.count')}}"
                                   value="{{$product->count}}"/>
                        </div><!-- /.form-group -->

                    </div>
                </div>
                <div class="row align-items-end col-12">
                    <div class="card-body d-flex flex-column flex-md-row align-items-md-end p-2">


                        <div class="form-group col-md-6 mx-2">
                            <label> {{__('msg.status')}} :
                                {{__('msg.active')}} <input type="radio" name="status" class="flat-red"
                                                            @if($product->status) checked @endif
                                                            value="1">
                            </label>
                            <label>
                                {{__('msg.inactive')}} <input type="radio" name="status" class="flat-red"
                                                              @if(!$product->status) checked @endif value="0">
                            </label>
                        </div>


                        <div class="form-group col-md-6 mx-2">
                            <label for="name">{{__('msg.description')}} </label>
                            <textarea id="mytextarea" name="description"> {{$product->description}} </textarea>
                        </div>

                    </div>
                </div>


                <div class="row align-items-end col-12">
                    <div class="card-body d-flex flex-column flex-md-row align-items-md-end p-2">


                        <div class="form-group col-md-6 mx-2">
                            <label>{{__('msg.product_category')}}</label>
                            <select class="form-control" name="category" required>
                                @foreach($productCategories as $productCategory)
                                    <option value="{{$productCategory->id}}"
                                            @if($product->product_category_id == $productCategory->id) selected @endif>
                                        {{$productCategory->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group col-md-6 mx-2">
                            <label for="avatar">{{__('msg.image')}} </label>
                            <img src="{{$product->image}}" width="100">
                            <input class="form-control" accept=".jpg,.jpeg,.png,.bmp" type="file" name="image"/>
                        </div><!-- /.form-group -->


                    </div>
                </div>


                <div class="input-group-btn">
                    <input type="submit" value="{{__('msg.submit')}}" class="btn btn-success" style="float: left">
                    <a href="{{route('seller.products.index')}}" class="btn btn-danger"
                       style="float: left">{{__('msg.cancel')}}</a>
                </div>


            </form>

        </div>
        <!-- /.box-body -->
    </div>

@endsection
@section('script')
    <script src="/admin-panel/bower_components/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea#mytextarea',
            plugins: 'advlist autolink link lists preview table code pagebreak',
            menubar: false,
            language: 'fa',
            height: 300,
            relative_urls: false,
            toolbar: 'undo redo | removeformat preview code | fontsizeselect bullist numlist | alignleft aligncenter alignright alignjustify | bold italic | pagebreak table link',
        });

    </script>




@endsection
