<?php


use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    auth()->logout();
    return view('welcome');
});
Route::get('home', function () {
    if (auth()->user()->is_admin)
        return redirect()->route('admin.home');
    if (auth()->user()->shop) {
        if (auth()->user()->shop->status)
            return redirect()->route('seller.home');
        else {
            session()->push('message', __('msg.for_login_to_panel_confirm_needed'));
            return redirect('/');
        }
    }else{
        return redirect('/');
    }
})->name('home');

Auth::routes();

Route::group(['namespace' => 'Seller', 'prefix' => 'seller-panel', 'middleware' => ['user_type:seller', 'auth'], 'as' => 'seller.'], function () {

    Route::get('home', function () {
        return view('seller-panel.dashboard');
    })->name('home');

    Route::resource('products','ProductController');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin-panel', 'middleware' => ['user_type:admin', 'auth'], 'as' => 'admin.'], function () {

    Route::get('home', function () {
        return view('admin-panel.dashboard');
    })->name('home');

    Route::resource('sellers','SellerController')->only(['index','edit','update']);
    Route::resource('product-categories','ProductCategoryController');
    Route::resource('products','ProductController')->only(['index' , 'destroy']);
    Route::post('products/confirm/{id}','ProductController@confirmProduct')->name('products.confirm');

});

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
